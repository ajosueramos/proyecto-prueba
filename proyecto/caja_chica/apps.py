from django.apps import AppConfig


class CajaChicaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'caja_chica'
