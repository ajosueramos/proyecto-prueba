from django.apps import AppConfig


class CuentasPagarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cuentas_pagar'
