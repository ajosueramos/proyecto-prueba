from django.apps import AppConfig


class FichaPersonalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ficha_personal'
