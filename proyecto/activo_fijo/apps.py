from django.apps import AppConfig


class ActivoFijoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'activo_fijo'
